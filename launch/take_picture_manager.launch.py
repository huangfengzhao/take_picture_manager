#!/usr/bin/env python3

import os
from launch import LaunchDescription
import launch_ros.actions
from launch.substitutions import LaunchConfiguration, PythonExpression
from launch.actions import (DeclareLaunchArgument, GroupAction,
                            IncludeLaunchDescription, SetEnvironmentVariable)
from ament_index_python.packages import get_package_share_directory
from nav2_common.launch import RewrittenYaml

def generate_launch_description():

    # Parameters
    camera_ns_arg = DeclareLaunchArgument(
        'camera_ns',
        default_value='/camera/preposition',
        description='Camera namespace')

    # Nodes launching commands
    take_picture_manager_cmd = launch_ros.actions.Node(
        package='take_picture_manager',
        executable='take_picture_manager',
        name="take_picture_manager",
        output='screen',
        emulate_tty=True, 
        remappings=[('save_camera', [LaunchConfiguration('camera_ns'),'/save_camera'])])

    ld = LaunchDescription()
    ld.add_action(camera_ns_arg)
    ld.add_action(take_picture_manager_cmd)


    return ld
