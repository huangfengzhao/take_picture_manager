

#include <iostream>
#include <chrono>
#include <functional>
#include <thread>
#include <atomic>
#include <memory>
#include <string>
#include <rclcpp/rclcpp.hpp>
#include <std_srvs/srv/empty.hpp>

#define UEVENT_BUFFER_SIZE 2048

class take_picture_manager
{
public:
    take_picture_manager(rclcpp::Node *node);
    void usb_detection_thread(void);
    void key_detection_thread(void);
    int  set_led_level(int num);
    int  check_usb_capacity();
    void client_req_callback();

private:
    rclcpp::Node *node;
    bool usb_insert_state = false;
    bool usb_capacity_state = false;
    bool clent_request_flag = false;
    int  duration_ms  = 1000; 
    std::string gpio_key = "/sys/class/gpio/gpio18/value";
    std::string gpio_led = "/sys/class/gpio/gpio19/value";
    std::string usb_mount_path = "/media/sda1";
    std::string usb_path = "/dev/sda1";
    rclcpp::TimerBase::SharedPtr client_timer;
    rclcpp::Client<std_srvs::srv::Empty>::SharedPtr client_take_picture;

};











