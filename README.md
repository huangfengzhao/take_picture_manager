# `take_picture_manager`

take picture manager

## Running the ROS2 node

```
ros2 launch take_picture_manager take_picture_manager.launch.py
```
##take_picture_manager功能描述

```
1. U盘插入检测、U盘容量检测、 未插入U盘、容量不足、禁止拍照、按下按键时led灯一闪 
（最好不要在开机状态拔插u盘、建议在开机前插入U盘、关机后再拔出u盘。避免热插拔造成U盘损坏） 

2. 按下按键 led灯两闪、表示全景相机服务未开启

3. 按下按键、led常亮表示拍照、拍照存放U盘 （按一下间隔2秒拍照一张、按照日期创建文件夹、按照日期命名存放）

4. 再次按下按键led变暗、表示停止拍照

```

gpio18 作为按键 
```
echo 18 > /sys/class/gpio/export
echo in > /sys/class/gpio/gpio18/direction
echo "rising" > /sys/class/gpio/gpio18/edge 

#cat /sys/class/gpio/gpio18/value
#echo 18 > /sys/class/gpio/unexport

```
gpio19 作为led 
```
echo 19 > /sys/class/gpio/export
echo out > /sys/class/gpio/gpio19/direction
echo 0 >  /sys/class/gpio/gpio19/value
#cat /sys/class/gpio/gpio19/value
#echo 19 > /sys/class/gpio/unexport

```

在/etc/rc.local末尾添加
```
echo 18 > /sys/class/gpio/export
echo in > /sys/class/gpio/gpio18/direction
echo "rising" > /sys/class/gpio/gpio18/edge
echo 19 > /sys/class/gpio/export
echo out > /sys/class/gpio/gpio19/direction

```


call camera service
```
ros2 service call /camera/panoramic/save_camera std_srvs/srv/Empty

```