#include "take_picture_manager.hpp"

class take_picture_node: public rclcpp::Node
{

public:
    take_picture_node():Node("take_picture_manager")
    {
        this->manager = new take_picture_manager(this);
    }

private:
   take_picture_manager *manager;
};


int main(int argc, char * argv[]){

  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<take_picture_node>());
  rclcpp::shutdown();
  return 0;

} // main
