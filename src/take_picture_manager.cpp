#include "take_picture_manager.hpp"
#include <ctype.h>
#include <sys/un.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <linux/types.h>
#include <linux/netlink.h>
#include <unistd.h>
#include <regex>
#include <iostream>
#include <fcntl.h>
#include <poll.h>
#include <dirent.h>
#include <sys/statfs.h>
#include<iostream>


#define GPIO_VALUE_H "1"
#define GPIO_VALUE_L "0"

using namespace std::chrono_literals;

take_picture_manager::take_picture_manager(rclcpp::Node *node)
{
    this->node = node;
    this->client_take_picture = node->create_client<std_srvs::srv::Empty>("save_camera");
    

    node->declare_parameter("duration_ms", 2000);
    node->get_parameter_or("duration_ms", duration_ms , 1000); //默认时间
    std::chrono::milliseconds intv(duration_ms);
    this->client_timer = node->create_wall_timer(intv, std::bind(&take_picture_manager::client_req_callback, this));
    this->check_usb_capacity();
    std::thread(&take_picture_manager::usb_detection_thread, this).detach();
    std::thread(&take_picture_manager::key_detection_thread, this).detach();
}


void take_picture_manager::client_req_callback(){

    if(this->clent_request_flag)
    {
        auto request = std::make_shared<std_srvs::srv::Empty::Request>(); 
        // 发起拍照请求、不需要响应
        auto result = client_take_picture->async_send_request(request);
    }
} 


void take_picture_manager::usb_detection_thread()
{
    struct sockaddr_nl client;
    struct timeval tv;
    int fd, rcvlen, ret;
    fd_set fdset;
    int buffersize = 1024;
    fd = socket(AF_NETLINK, SOCK_RAW, NETLINK_KOBJECT_UEVENT);
    memset(&client, 0, sizeof(client));
    client.nl_family = AF_NETLINK;
    client.nl_pid = getpid();
    client.nl_groups = 1; /* receive broadcast message*/
    setsockopt(fd, SOL_SOCKET, SO_RCVBUF, &buffersize, sizeof(buffersize));
    bind(fd, (struct sockaddr*)&client, sizeof(client));
    while (1) {
        char buf[UEVENT_BUFFER_SIZE] = { 0 };
        FD_ZERO(&fdset);
        FD_SET(fd, &fdset);
        tv.tv_sec = 0;
        tv.tv_usec = 100 * 1000;
        ret = select(fd + 1, &fdset, NULL, NULL, &tv);
        if(ret < 0)
            continue;
        if(!(ret > 0 && FD_ISSET(fd, &fdset)))
            continue;
        /* receive data */
        rcvlen = recv(fd, &buf, sizeof(buf), 0);
        if (rcvlen > 0) {
            
            // RCLCPP_INFO(node->get_logger(), "recv buf: %s",buf);
            std::cmatch regex_result;
            if(std::regex_match(buf,regex_result,std::regex("(\\w+)@.*?/block/(s\\w{2})")))
            {
                for(auto x=regex_result.begin()+1;x!=regex_result.end();++x)
                {
                    const char *action = x->str().c_str();
                    // RCLCPP_INFO(node->get_logger(), "-------------: %s",action);
                    if (!x->str().empty() && strstr(action, "add") != 0 )
                    {
                        RCLCPP_INFO(node->get_logger(), "插入u盘: %s",action);
        
                        sleep(2);  //等待u盘挂载

                        check_usb_capacity();

                    }else if (!x->str().empty() && strstr(action, "remove") != 0)
                    {
                        RCLCPP_INFO(node->get_logger(), "remove usb: %s",action);
                        this->usb_insert_state = false;
                        this->usb_capacity_state = false;
                    }
                    RCLCPP_INFO(node->get_logger(), "usb_parse_data: %s",buf);
                }
                
            }
        }

    //    RCLCPP_INFO(node->get_logger(), "usb_detection_thread ");
    }
    close(fd);

}
void take_picture_manager::key_detection_thread()
{
 
    int fd=open(gpio_key.c_str(),O_RDONLY);
    if(fd<0)
    { 
        RCLCPP_ERROR(node->get_logger(), "open %s failed! ",gpio_key.c_str());
        return;
    }
    struct pollfd fds[1];
    fds[0].fd=fd;
    fds[0].events=POLLPRI;

    int timeout_ms = 3000;  // 超时时间, ms.
    auto tp = std::chrono::steady_clock::now();
    while(1){

        if(poll(fds,1,-1)==-1){
            RCLCPP_ERROR(node->get_logger(), "poll failed!");
            return;
        }


        if(fds[0].revents&POLLPRI)
        {
            if(lseek(fd,0,SEEK_SET)==-1)
            {
                RCLCPP_ERROR(node->get_logger(), "lseek failed!");
                return;
            }
            char buffer[16];
            int len;
            if((len=read(fd,buffer,sizeof(buffer)))==-1)
            {
                RCLCPP_ERROR(node->get_logger(), "read failed!");
                return;
            }
            buffer[len]=0;


            auto intv = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now()-tp).count();
                                // 检测超时退出.
            if (intv < timeout_ms) 
            {
                //RCLCPP_ERROR(node->get_logger(), "抖动不处理");
                continue;
            }
            tp = std::chrono::steady_clock::now();
            if(this->usb_insert_state == true && this->usb_capacity_state == true)
            {
                //检查全景相机服务是否开启，工作是否正常
                if (!client_take_picture->wait_for_service(1s)) {

                    //全景相机服务未开启、工作异常设置led二闪警告
                    this->set_led_level(1010);
                    this->clent_request_flag = false;
                    RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "service not available, waiting again...");
                    continue;
                }

                //全景相机服务已开启、拍照常亮、否则常灭
           
                this->clent_request_flag = !clent_request_flag;
                this->set_led_level(this->clent_request_flag);
                RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "client sends take picture request %d",this->clent_request_flag );
                // auto request = std::make_shared<std_srvs::srv::Empty::Request>(); 
                // // 发起拍照请求、不需要响应
                // auto result = client_take_picture->async_send_request(request);
            }
            else
            {
                RCLCPP_ERROR(node->get_logger(), "The usb is not inserted, the usb is not mounted, the usb space is insufficient, or the usb file format is incorrect");
                //设置led一闪告警
                this->set_led_level(010);
                this->clent_request_flag = false;
            }

        }

      //  RCLCPP_INFO(node->get_logger(), "key_detection_thread ");
    }
}


int take_picture_manager::set_led_level(int num)
{
    int fd; 
    fd = open(gpio_led.c_str(), O_WRONLY);
    if(fd == -1){
        RCLCPP_ERROR(node->get_logger(), "open %s failed!",gpio_led.c_str());
        return -1;
    }
    switch (num)
    {
        case 0:
            write(fd, GPIO_VALUE_L,sizeof(GPIO_VALUE_L));  //常暗
            break;
        case 1:
            write(fd, GPIO_VALUE_H,sizeof(GPIO_VALUE_H));  //常亮
            break;
        case 010:
            write(fd, GPIO_VALUE_L,sizeof(GPIO_VALUE_L));  //一闪
            sleep(1);
            write(fd, GPIO_VALUE_H,sizeof(GPIO_VALUE_H)); 
            sleep(1);
            write(fd, GPIO_VALUE_L,sizeof(GPIO_VALUE_L)); 
            break;
        case 1010:   
            write(fd, GPIO_VALUE_H,sizeof(GPIO_VALUE_H));  //二闪
            sleep(1);
            write(fd, GPIO_VALUE_L,sizeof(GPIO_VALUE_L)); 
            sleep(1);
            write(fd, GPIO_VALUE_H,sizeof(GPIO_VALUE_H)); 
            sleep(1);
            write(fd, GPIO_VALUE_L,sizeof(GPIO_VALUE_L)); 
            break;

        default:
            break;
    }
    close(fd); 
    return 0;
}


int  take_picture_manager::check_usb_capacity()
{
    if(access(usb_mount_path.c_str(),0) == -1 || access(usb_path.c_str(),0) == -1 )
    {
        //不存在、usb未挂载上
        RCLCPP_ERROR(node->get_logger(), "usb umount error,check the usb file format");
        this->usb_insert_state = false;
        this->usb_capacity_state = false;
        return -1;
    }
    struct statfs disk_statfs;
    double free=0,total=0, percent=0;
    if(statfs(usb_mount_path.c_str(),&disk_statfs) == 0)
    {
        free = (disk_statfs.f_bsize * disk_statfs.f_bfree) / (1024*1024*1024.0);
        total = (disk_statfs.f_bsize * disk_statfs.f_blocks) / (1024*1024*1024.0);
        percent = free/total*100;
        if(percent < 1) //usb 空间不足
        {
            RCLCPP_ERROR(node->get_logger(), "usb %s lack of space",usb_mount_path.c_str());
            this->usb_capacity_state = false;
            return -2;
        }
    }

    //usb正常、已插入、已挂载、容量足够
    this->usb_insert_state = true;
    this->usb_capacity_state = true;
    // printf("u盘剩余空间: %.2f\n",free );
    // printf("u盘总空间:   %.2f\n",total );
    // printf("u盘剩余空间百分比: %0.2f%%\n",percent);
    return 0;
}
