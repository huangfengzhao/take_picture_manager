#!/bin/bash

# systemctl enable novabot_launch.service
# systemctl start  novabot_launch.service

# /etc/rc.local
# echo 18 > /sys/class/gpio/export
# echo in > /sys/class/gpio/gpio18/direction
# echo "rising" > /sys/class/gpio/gpio18/edge
# echo 19 > /sys/class/gpio/export
# echo out > /sys/class/gpio/gpio19/direction

source /opt/ros/galactic/setup.bash
source /userdata/take_picture/install/setup.bash
export ROS_LOG_DIR=/userdata/take_picture/novabot_log/
source /userdata/take_picture/install/setup.bash
export LD_LIBRARY_PATH=/usr/lib/hbmedia/:/usr/lib/hbbpu/:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/usr/local/lib:/usr/lib/aarch64-linux-gnu:/usr/bpu:/usr/opencv_world_4.6/lib:$LD_LIBRARY_PATH

case "$1" in
start)
  echo "starting camera.... "

  killall -q -9 camera_307_preposition
  killall -q -9 take_picture_manager
  sleep 2s

  ros2 launch take_picture_manager take_picture_manager.launch.py&
  ros2 launch camera_307_preposition camera_307_preposition_node.launch.py&
  ;;
stop)
  echo "stopping.... "

  killall -q -9 camera_307_preposition
  killall -q -9 take_picture_manager
  ;;
*)
  echo "Usage: $0 {start|stop|}"
  exit 1
  ;;
esac
